document.addEventListener('DOMContentLoaded', function(){

var newsFeed = document.getElementById("newsFeed");
newsFeed.classList.add("_posts");


var postAuthor = document.getElementById("postAuthor");
var postText = document.getElementById("postText");
var imageUrl = document.getElementById("imageUrl");
var inputFile = document.getElementById("inputFile");
var btnPost = document.getElementById("btnPost");
var preview = document.getElementById("preview");
var previewBlock = document.getElementById("previewBlock")
var deleteButt = document.getElementById("deleteButt");

//Функция-конструктор для поста
function Post(person, text, image, picture) {
  this.author = person;
  this.date = new Date();
  this.text = text;
  this.image = image;
  this.picture = picture;
  // this.btn = btn;
}



var input = document.getElementById("inputFile");
var reader = new FileReader();
  input.addEventListener('change', function(e){
    console.log(input.files)
    

    reader.onload = function(){
      var img = new Image();
      img.src = reader.result;
      img.style.cssText = "width: 100%; max-height: 260px;"
      while(preview.firstChild){
      	preview.removeChild(preview.firstChild);
      }
      preview.appendChild(img);


      deleteButt.removeAttribute('disabled');
      deleteButt.addEventListener('click', function(){
      	preview.removeChild(img);
      	deleteButt.disabled = "true";
      })
      
    }
    reader.readAsDataURL(input.files[0])
  }, false)


btnPost.addEventListener('click', function(e){
	e.preventDefault();

	var pic = new Image();
	reader.onload = function(){
      pic.src = reader.result;
  	}
	
	var author = postAuthor.value;
	var text = postText.value;
	var pictureSrc = imageUrl.value;
	var pictureSrc2 = reader.result;

	var newPost = new Post(author, text, pictureSrc, pictureSrc2);


	var post = document.createElement("div");
	post.classList.add("_post");
	var nameAuthor = document.createElement("h4");
	var postDate = document.createElement("time");
	var storyText = document.createElement("p");
	var image = document.createElement('img');
	var picture = document.createElement('img');

	

	var post_person = newPost.author;
	var post_date = newPost.date.toLocaleDateString()+' ('+newPost.date.toLocaleTimeString()+')';
	var post_text = newPost.text;
	var post_image = newPost.image;
	var post_picture = newPost.picture;
	
	// var btn = btnLike;

	nameAuthor.innerText = post_person;
	post.appendChild(nameAuthor);
	postDate.innerText = post_date;
	post.appendChild(postDate);
	var btnDelete = document.createElement('button');
	btnDelete.setAttribute("class", "btn");
 	btnDelete.innerText = 'delete post';
  	post.appendChild(btnDelete);
	storyText.innerText = post_text;
	post.appendChild(storyText);
	image.src = post_image;
	image.classList.add("images");
	post.appendChild(image);
	picture.src = post_picture;
	picture.classList.add("images");
	post.appendChild(picture);

	btnDelete.onclick = function(){
    	btnDelete.parentElement.style.display = 'none'
  	}

	

	var btnLike = document.createElement('button');
  	btnLike.innerText = 'I like it!';
  	post.appendChild(btnLike);
  	var showLikes = document.createElement('p');
  	btnLike.style.cssText = "width: 100px;";
  	btnLike.appendChild(showLikes);
  	
  	var count = 0;
  	btnLike.innerHTML = '<p>I like it! +' + count + "  " + "<img src=https://e.unicode-table.com/orig/79/f3e6a4ad4fde8414ffc5cf07a16d37.png style='width:15px; height:15px;'>" +'</p>';
  	btnLike.onclick = function(){
    count++;
    btnLike.innerHTML = '<p>I like it! +' + count + " " + "<img src=https://e.unicode-table.com/orig/79/f3e6a4ad4fde8414ffc5cf07a16d37.png style='width:15px; height:15px;'>" +'</p>';
    
	// var string = JSON.stringify(newPost);
	// console.log(string);

	}
	newsFeed.appendChild(post);



var btnComment = document.createElement('button');
  btnComment.innerText = 'Write comment';
  btnComment.setAttribute("class", "btn");
  post.appendChild(btnComment);

  btnComment.onclick = function(){
  	var newComment = document.createElement("div");

    var newAuthor = document.createElement('input');
    newAuthor.placeholder = 'Author';
    newAuthor.style.cssText = 'width: 200px; margin-top: 30px; margin-left:20px; background: #a6acb5;'
    var newText = document.createElement('textarea');
    newText.placeholder = 'Your comment';
    newText.style.cssText = 'width: 300px; min-height: 80px; margin-top: 15px; margin-left:20px; background: #a6acb5;'
    var newButton = document.createElement('button');
    newButton.innerText = 'Send';
    newButton.style.cssText = 'margin-top: 10px; padding: 10px;';

    newComment.appendChild(newAuthor);
    newComment.appendChild(newText);
    newComment.appendChild(newButton);

    post.appendChild(newComment);
    

    newButton.onclick = function(){
      var getComment = document.createElement("div");
      var hr = document.createElement("hr");
      var getAuthor = document.createElement("h5");
      getAuthor.innerText = newAuthor.value;
      var getText = document.createElement("p");
      getText.innerText = newText.value;
      getComment.appendChild(getAuthor);
      getComment.appendChild(getText);
      getComment.style.cssText = "font-color: black; font-size: 17px";
      getComment.appendChild(hr);
      post.appendChild(getComment);

      // newComment.removeChild(newButton);
      post.removeChild(newComment);
    }
  
  }

var string = JSON.stringify(newPost);
console.log(string);
// localStorage.setItem('information', string);
})








});